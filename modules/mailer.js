const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const path = require('path');

const {host, port, user, pass} = require('../config/mail.json');

const transport = nodemailer.createTransport({
    host,
    port,
    auth: {user, pass},
  });

  transport.use('compile', hbs({
      viewEngine:{
          extname: '.html',
          layoutsDir: 'resources/mail/',
          defaultLayout:'forgot_password',
          partialDir:'resources/mail/',
      },
      viewPath: path.resolve('./resources/mail/'),
      extName: '.html',
  }))

  module.exports = transport;