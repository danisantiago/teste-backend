const express = require('express');
const bodyParser = require('body-parser');

//Configurando o banco
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://ioasys:root123@cluster0.ign9z.mongodb.net/ioasys?retryWrites=true&w=majority' ,{
    useNewUrlParser:true,
    useUnifiedTopology:true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Erro na Ligação ao MongoDB'));

const user = require('./api/routes/UserRoute');
const movie = require('./api/routes/MovieRoute');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/user', user);
app.use('/movie', movie);

let port = 8088;

app.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}`)
});