const jwt = require('jsonwebtoken');
const authConfig = require('../../config/auth.json');

module.exports = (req, res, next) =>{
    const auth = req.headers.authorization;

    if(!auth)
        return res.status(401).send({message: 'Informe o Authorization'});

    //verificando o formato do token (Bearer + hash)
    const parts = auth.split(' ');
    if(!parts.length === 2)
        return res.status(401).send({message: 'Formato de token inválido'});

    const [scheme, token] = parts;

    if(!/^Bearer$/i.test(scheme))
        return res.status(401).send({message: 'Formato de token inválido'});

    jwt.verify(token, authConfig.secret, (err, decoded) => {
        if(err) return res.status(401).send({message: 'Token inválido'});

        req.userId = decoded.params.id;
        return next();
    });
}