const mongoose = require('mongoose')

const MovieSchema = new mongoose.Schema({
    name: {type: String, required:true},
    director: {type: String, required:true},
    writers: [String],
    stars: [String],
    genre: [String],
    rating: {type: Number, default:0},
    numberOfVotes: {type: Number, default:0}
})

module.exports = mongoose.model('Movie', MovieSchema);