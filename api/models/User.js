const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    name: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true, select:false},
    isAdmin: {type: Boolean, required:true, default:false},
    status:{type:Boolean, required:true, default:true},
    passwordResetToken:{type: String, select:false},
    passwordResetExpire:{type:Date, select:false}
});

module.exports = mongoose.model('User', UserSchema);