const express = require('express');
const router = express.Router();
const authMiddleware = require('../middleware/auth');

const movie_controller = require('../controllers/MovieController');

router.post('/register', authMiddleware, movie_controller.insert);

router.patch('/:movieId/vote', authMiddleware, movie_controller.vote);

router.get('', movie_controller.search);

router.get('/:movieId/detail', movie_controller.detail);

module.exports = router;