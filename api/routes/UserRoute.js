const express = require('express');
const router = express.Router();
const authMiddleware = require('../middleware/auth');


const user_controller = require('../controllers/UserController');

router.post('/register', user_controller.insert);

router.get('/', user_controller.findAll);

router.post('/login', user_controller.auth);

router.put('/edit/:userId', authMiddleware, user_controller.update);

router.put('/delete/:userId', authMiddleware, user_controller.delete);

router.post('/forgot-password', user_controller.forgotPass);

router.post('/reset-password', user_controller.resetPass);

module.exports = router;