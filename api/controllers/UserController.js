var User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const mailer = require('../../modules/mailer');

const authConfig = require('../../config/auth.json');

exports.findAll = (req, res) =>{
    User.find()
    .then(response => res.json(response))
    .catch(err => res.status(500).send({message: "Erro ao pesquisar todos os usuários"}));
}

function generateToken(params ={}){
    return jwt.sign({params}, authConfig.secret, {
        expiresIn:86400 //expira em um dia
    });
}

exports.insert = async (req, res) =>{
   //validando a requisicao
   if(!req.body.name || !req.body.email || !req.body.password){
        res.status(400).send({message: "Todos os campos precisam ser preenchidos!"});
        return;
   }

   User.findOne({'email':req.body.email})
    .then(async user => {
        if(user){
            res.status(400).send({message: "Esse email já foi cadastrado!"});
        
        }else{
           await bcrypt.hash(req.body.password, 10)
                .then(hash => {
                    let encryptedPassword = hash;

                    let user = new User(
                        {
                            name: req.body.name,
                            email: req.body.email,
                            password: encryptedPassword,
                            isAdmin: req.body.isAdmin
                        }
                    );

                    user.save()
                    .then(() => {
                        user.password = undefined;
                        res.status(201).send({user, token:generateToken({id: user.id})})
                    }) 
                    .catch(err => res.status(500).send({message: "Erro ao cadastrar o usuário."}));
                })
                .catch(err => res.status(500).send({message: "Erro ao criptografar a senha."}));
        }
    })
         
}

exports.auth = async (req, res) =>{
    const {email, password} = req.body;

    const user = await User.findOne({email}).select('+password');

    if(!user)
        return res.status(400).send({message: "Usuário não encontrado!"})

    if(!await bcrypt.compare(password, user.password))
        return res.status(400).send({message: "Senha inválida!"});

    user.password = undefined;

    const token = generateToken({id: user.id});

    res.send({user, token});
}

exports.update = async (req, res) =>{
    const {name, email, password} = req.body;
    
    const user = await User.findById(req.params.userId);

    const userJwt = await User.findById(req.userId);

    if(!user || !user.status){
        return res.status(404).send({message: "Usuário não encontrado"});
    }else{
        if(user._id != userJwt._id && !userJwt.isAdmin){
            return res.status(403).send({message: "Acesso negado"})
        }else{
            if(!userJwt.isAdmin){
                const newUser = await User.findByIdAndUpdate(req.params.userId, {name}, {new: true});
                await newUser.save()
                .then(() => {
                    newUser.password = undefined;
                    res.status(200).send({newUser});
                }) 
                .catch(err => res.status(400).send({message: "Erro ao atualizar o usuário."}));
            
                
            }else{ //se o usuario do token for adm ele pode mudar as informações de outro usuario
                if(email != user.email){
                    const emailInvalido = await User.findOne({'email': email});
                    if(emailInvalido && emailInvalido.status)
                        return res.status(400).send({message: "Esse email já está sendo utilizado por outro usuário."})
                }

                let encryptedPassword = await bcrypt.hash(password, 10);
                const newUser = await User.findByIdAndUpdate(req.params.userId, {name, email, password: encryptedPassword}, {new: true});
                await newUser.save()
                .then(() => {
                    newUser.password = undefined;
                    res.status(200).send({newUser});
                }) 
                .catch(err => res.status(400).send({message: "Erro ao atualizar o usuário."}));
            }
        
        }
    }    
}

exports.delete = async (req, res) => {

    const user = await User.findById(req.params.userId);
    const userJwt = await User.findById(req.userId);  

    if(!user || !user.status){
        return res.status(404).send({message: "Usuário não encontrado"});
    
    }else{
        var id = Buffer.from(user._id.toString());
        var jwt = Buffer.from(userJwt._id.toString());
        if(id.equals(jwt) || userJwt.isAdmin){
            const deleteUser = await User.findByIdAndUpdate(req.params.userId, {'status':false}, {new: true});
            await deleteUser.save()
            .then(() => res.status(200).send({message: "Usuário deseativado com sucesso."})) 
            .catch(err => res.status(400).send({message: "Erro ao desativar o usuário."}));
        }else{
            return res.status(403).send({message: "Acesso negado"});
        }
    }
}

exports.forgotPass = async (req, res) => {
    const {email} = req.body;

    try {
        const user = await User.findOne({'email': email});

        if(!user)
            return res.status(400).send({message: "Usuário não encontrado."})

        const token = crypto.randomBytes(10).toString('hex');

        var now = new Date();
        now.setHours(now.getHours() + 1);

        await User.findByIdAndUpdate(user._id, {
            '$set': {
                passwordResetToken: token,
                passwordResetExpire: now
            }
        });

        mailer.sendMail({
            to: email,
            from: 'danii.santiago@gmail.com',
            template: 'forgot_password',
            context: {token},
        }, (err) =>{
            if(err){
                console.log(err);
                return res.status(500).send({message: "Erro no envio do email de recuperação de senha"}); 
            }
        })

        return res.send();

    } catch (error) {
        res.status(500).send({message: "Erro na recuperação de senha"});
    }
}

exports.resetPass = async (req, res) =>{
    const {email, token, password} = req.body;

    try {

        const user = await User.findOne({'email': email}).select('+passwordResetToken passwordResetExpire');

        if(!user)
            return res.status(400).send({message: "Usuário não encontrado."});

        if(token !== user.passwordResetToken)
            return res.status(400).send({message: "Token inválido"});

        const now = new Date();

        if(now > user.passwordResetExpire)
            return res.status(400).send({message: "Token expirado"});
        

        user.password = await bcrypt.hash(password, 10);
        await user.save();

        res.send();
                
    } catch (error) {
        res.status(500).send({message: "Erro no reset da senha"});
    }
}


