var Movie = require('../models/Movie');
var User = require('../models/User');

exports.insert = async (req, res) => {
    
    const{name, director, writers, stars, genre} = req.body;

    if(!name || !director || !writers || !stars || !genre)
        return res.status(400).send({message: "Todos os campos precisam ser preenchidos."});

    const user = await User.findById(req.userId);
    if(!user.isAdmin)
        return res.status(403).send({message: "Acesso negado"});
    
    //caso o filme possua o mesmo nome e diretor não poderá ser cadastrado
    const findMovie = await Movie.find().and([{name: name}, {director: director}]);
    if(findMovie.length != 0){
        return res.status(400).send({message: "Esse filme já foi cadastrado"});
    
    }else{
        let movie = new Movie(
            {
                name: name,
                director: director,
                writers: writers,
                stars: stars,
                genre: genre
            }
        );
    
        movie.save()
        .then(() => res.status(201).send({movie}))
        .catch(err => res.status(500).send({message: "Erro ao cadastrar o filme."}));
    }
    
}

exports.vote = async (req, res) => {

    const{vote} = req.body;

    if(vote < 0 || vote > 4 )
        return res.status(400).send({message: "O voto precisa ser entre 0 e 4"});

    const user = await User.findById(req.userId)
    if(user.isAdmin)
        return res.status(400).send({message: "Usuários administradores não podem votar nos filmes."});

    const varMovie = await Movie.findById(req.params.movieId);

    var numberOfVotes = varMovie.numberOfVotes + 1;
    var rating = (varMovie.rating + vote) / numberOfVotes;
    
    const movie = await Movie.findByIdAndUpdate(req.params.movieId, {numberOfVotes, rating}, {new: true});
    if(movie)
        return res.status(200).send({movie});

    
    return res.status(404).send({message: "Nenhum filme com esse identificador foi encontrado."});

}

exports.search = async(req, res) => {
    const{director, name, genre, stars} = req.query;

    var movies = await Movie.find();

    if(movies.length < 1)
        return res.status(404).send({message:"Nenhum filme cadastrado no banco de dados"});
        
    if(!director && !name && !genre && !stars)
        return res.status(200).send({movies});    
    
    await Movie.find({$or:[{
                        name: {$regex: (name)?name:0, $options: 'i'}},
                        {director: {$regex: (director)?director:0, $options: 'i'}},
                        {stars: {$regex: (stars)?stars:0, $options: 'i'}},
                        {genre: {$regex: (genre)?genre:0, $options: 'i'}}
                    ]})
    .then(response => {
        if(response.length > 0)
            return res.status(200).json(response)
        return res.status(404).send({message:"Nenhum filme encontrado"});
    })
    .catch(err => res.status(500).send({message: "Erro ao listar os filmes"}));
}

exports.detail = async(req, res) => {

    const movie = await Movie.findById(req.params.movieId);

    if(!movie)
        return res.status(404).send({message: "Não foi encontrado nenhum filme com esse identificador"});

        var text = `Detalha do filme: ${movie.name} 
        Diretor: ${movie.director} 
        Gêneros: ${movie.genre} 
        Atores: ${movie.stars}
        Classificação(0-4): ${movie.rating}  `;

    res.status(200).send(text);
                   
}